import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios"
import { RootState } from "../../app/store";

// interfaceとりあえずここに置いた。別ディレクトリ管理でもいいと思う
interface User {
  "id": number,
  "name": string,
  "username": string,
  "email": string,
  "address": {
  "street": string,
  "suite": string,
  "city": string,
  "zipcode": string,
  "geo": {
  "lat": string,
  "lng": string
    }
  },
  "phone": string,
  "website": string,
  "company": {
    "name": string,
    "catchPhrase": string,
    "bs": string
  }
}

interface UsersState {
  users: User[]
}

const initialState: UsersState = {
  users: []
}

// API通信部分
const apiUrl = "https://jsonplaceholder.typicode.com/users"

export const fetchAsyncGet = createAsyncThunk("fetch/get", async() => {
  const response = await axios.get(apiUrl)
  return response.data
})

const fetchSlice = createSlice({
  name: "fetch",
  initialState,
  reducers: {},
  // API通信の後処理を書く
  extraReducers: (builder) => {
    // fullfilled→成功時の処理・rejected→失敗時の処理
    builder.addCase(fetchAsyncGet.fulfilled, (state, action) => {
      return {
        ...state,
        users: action.payload // action.payloadにfetchAsyncGetの戻り値(=API通信のレスポンス)が格納されている
      }
    })
  }
})

export const selectUsers = (state: RootState) => state.fetch.users // コンポーネントにreducerで取得したユーザー情報を渡す
export default fetchSlice.reducer