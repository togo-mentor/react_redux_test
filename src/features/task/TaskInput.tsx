import React from 'react'
// import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { newTask } from './taskSlice'
import { useForm, SubmitHandler } from 'react-hook-form';
import styles from './taskInput.module.css'

// フォームの入力値のstate
interface InputState  {
  title: string;
};

export const TaskInput = () => {
  const dispatch = useDispatch()

  // state管理はuseFormが一元的にやってくれるので不要そう
  // const [title, setTitle] = useState<string>("")

  // const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   setTitle(e.target.value)
  // }

  // const addTask = (e: React.FormEvent) => {
    // e.preventDefault()
    // dispatch(newTask(title))
    // setTitle("")
  // }

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm<InputState
  >(
    {
      mode: 'onSubmit',
      reValidateMode: 'onChange', // 入力するたびに検証する
      defaultValues: {
        title: '' // 編集画面などで初期値を設定したい場合にここに入れる
      }
    }
  );

  const onSubmit: SubmitHandler<InputState> = (data) => {
    // preventDefaultは自動でかかる
    dispatch(newTask(data.title)) // dispatchしてredux storeに値を格納
    console.log('onSubmit:', data);
  }
  console.log('watch:', watch('title')); // watchは引数に渡した名前の入力値を監視する

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          type="text"
          // onChange={handleTitleChange} 
          {...register('title', { 
            required: '必須項目です', // messageだけ入れるとtrue扱い
            maxLength: {
              value: 10,
              message: '10文字以内で入力してください'
            },
          })} // formに要素を登録する。第2引数で検証パターンを指定
          placeholder="ここにタイトルを入力してね"
          className={styles.input}
        />
        {errors.title && (
          // errorsがある場合に表示されるテキスト
          <>
            <span style={{ color: 'red' }} className={styles.error}>{errors.title.message}</span>
          </>
        )}
        <button>NEW TASK</button>
      </form>
    </div>
  )
}
