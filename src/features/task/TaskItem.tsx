import React from 'react'
import { useDispatch } from 'react-redux'
import { completeTask, deleteTask, TaskState } from './taskSlice';

interface Props {
  task: TaskState
}

export const TaskItem = ({task}: Props) => {
  const dispatch = useDispatch()

  return (
    <div>
      <input type='checkbox' onClick={() => dispatch(completeTask(task))} defaultChecked={task.completed}/>
      <span>{task.title}</span>
      <button onClick={() => dispatch(deleteTask(task))}>DELETE</button>
    </div>
  )
}
