import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface TaskState {
  id: number
  title: string
  completed: boolean;
}

export interface TasksState {
  idCount: number
  tasks: TaskState[]
}

// 初期値
const initialState: TasksState = {
  idCount: 3,
  tasks: [
    {
      id: 3,
      title: 'task C',
      completed: false
    },
    {
      id: 2,
      title: 'task B',
      completed: false
    },
    {
      id: 1,
      title: 'task A',
      completed: false
    },
  ]
};

// storeの中のカウンターに関する状態管理をする場所
export const taskSlice = createSlice({
  name: 'counter',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    // アクションを定義
    // タスクを増やす
    newTask: (state, action) => {
      state.idCount += 1;
      const newItem = {
        id: state.idCount,
        title: action.payload,
        completed: false,
      }
      state.tasks = [newItem, ...state.tasks]
    },
    // タスクを完了させる
    completeTask: (state, action) => {
      const task = state.tasks.find((t) => t.id === action.payload.id)
      if (task) {
        task.completed = !task.completed
      }
    },
    // タスクを削除する
    deleteTask: (state, action) => {
      state.tasks = state.tasks.filter((t) => t.id !== action.payload.id)
    }
  },
});

export const { newTask, completeTask, deleteTask } = taskSlice.actions; 
// actionをexportしてコンポーネントから呼び出せるようにする

// 現在のタスクを取得する
export const selectTasks = (state: RootState) => state.tasks

export default taskSlice.reducer;
