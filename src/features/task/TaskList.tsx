import React from 'react'
import { useSelector } from 'react-redux'
import { selectTasks, TaskState } from './taskSlice';
import { TaskItem } from './TaskItem'

export const TaskList = () => {
  const tasks = useSelector(selectTasks)

  return (
    <>
      {tasks.tasks.map((task: TaskState) => {
        return (
          <TaskItem key={task.id} task={task} />
        )
      })}
    </>
  )
}
